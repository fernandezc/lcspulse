# **********************************************************************************
# MQSQcorrRN.py
# **********************************************************************************
# Authors:
# 	Christian Fernandez & Eddy Dib at LCS, ENSICAEN, Caen, France
# 
# Purpose:
# 	Generation of homonuclear MQ/SQ correlation experiment 
#   using RN symmetry sequences RN_n^u
#   
# Creation date:
#   17/10/2015
#
# Version:
#   0.1
#
# *********************************************************************************

PPTEXT = """;--------------------------------------------------------------------------------
; MQSQRN_n_nu.ppy
;
; Pulse program generated using "xpy MQSQcorrRN.py" command
;
; written by ED & CF
; LCS_Caen (17/10/2015)
;
; Pulse sequence for MQ/SQ correlation experiment
; using a R symmetry sequence (RN_n^nu) without supercycling.
;
; R element is (90)_0 (270)_180
;
;-------------------------------------------------------------------------------
;d1 : recycle delay
;ns : number of scans
;cnst31 : MAS rotation frequency in Hz
;cnst1 : R element phase
;p1 : 90 degree pulse (F1)
;pl1 : 90 degres pulse power (F1)
;l1 : N symmetry number (= 14)
;l2 : n symmetry number (= 4)
;l3 : nu symmetry number (= 5)
;l4 : coherence selected
;l30: number of pi equivalent in a R block
;p11 : length of R element 90 filp angle pulse
;pl11 : R sequence power - F1 (nuR*N/2n )
;p12 : length of R element 270 flip angle pulse
;d11 : length of excitation period
;l11 : excitation: no. pairs of R elements (see d11)
;d12 : length of reconversion period
;l12 : reconversion: no. pairs of R elements (see d12)
;d10 : Z-filter delay
;d31 : rotor period
;
;-------------------------------------------------------------------------------

;set l12=l11 for symmetric incrementation of number of R cycles
"l12=l11"

;calculate length of one rotor period
"d31 = 1.0s/cnst31"

;calculate length of excitation and reconversion periods: nu_R * n * l11
"d11 = l11*d31*l2"
"d12 = l12*d31*l2"

;calculate the 90 and 270 pulse lengths for R element
define pulse pul90
"pul90 = (2*0.25*d31*l2/l1)/l30"

define pulse pul270
"pul270 = (2*0.75*d31*l2/l1)/l30"

;cnst11 : to adjust t=0 for acquisition, if digmod = baseopt
"acqt0=1u*cnst11"

;calculate loop counters for R cycle loops
define loopcounter halfN
"halfN = l1/2"

"in0=inf1"	; Statement required with TOPSPIN 3.2

"cnst4 = l4"
"cnst3 = l3"

; Start sequence
;-------------------------------------------------------------------------------
1 ze

;RECYCLE DELAY
2 d1
  d31

;MQ EXCITATION (on F1) - R sequence
3 (pul90  pl11 ph11):f1
  (pul270 pl11 ph12):f1
  (pul90  pl11 ph13):f1
  (pul270 pl11 ph14):f1

; repeat this N/2 times (total duration n rotor periods)
  lo to 3 times halfN

; repeat this l11 times (buildup)
  lo to 3 times l11

;MQ EVOLUTION
  d0

;MQ RECONVERSION (on F1) - R sequence
4 (pul90  pl11 ph21):f1
  (pul270 pl11 ph22):f1
  (pul90  pl11 ph23):f1
  (pul270 pl11 ph24):f1

; repeat this N/2 times (total duration n rotor periods)
  lo to 4 times halfN

; repeat this l12 times
  lo to 4 times l12

;Z-FILTER (no decoupling)
  d10

;OBSERVATION PULSE (on F1)
  (p1 pl1 ph1):f1

;ACQUISITION
  go=2 ph31

"""
DEBUG = False

def calcRNparam(nu_R, N=14, n=4, plw_ref=20, p90_ref=2.2):
    """
    Compute PLW for a R N_n^u excitation block

    plw_ref and p90_ref are the default reference power/p90 for 1.9 mm probe
    must be adapted to the actual probe specifications

    """

    # power
    nu_rf = nu_R * N /  n
    p = 250.0e3/nu_rf
    plw = plw_ref * (p90_ref / p)**2

    if DEBUG:
        MSG(
            "nu_R:%dkHz    N:%d n:%d     ->    nu_rf:%.1fkHz (PLW_11:%.2f "
            "Watts)" % (
                nu_R * 1.0e-3, N, n, nu_rf * 1.0e-3, plw))
    return plw


# compute phase lists and append to pulse program pp = pp
def calcPhaseLists(N, n, nu, mq, phase0):

    ph11 = []
    ph12 = []
    ph13 = []
    ph14 = []
    ph21 = []
    ph22 = []
    ph23 = []
    ph24 = []
    
    to360 = 65536.
    to180 =  to360/2.  # because we cannot use floats

    ip0 = round(to360 / (4. * float(mq)))
    
    ph1 = [0, 1, 2, 3]
        
    ph = float(nu) * to180 / float(N)
    ph11.append(ph)
    ph12.append(ph + to180)
    ph13.append(-ph)
    ph14.append(-ph + to180)

    for i in range(mq * 2):   
        ph20 = float(i) * ip0 * 2
        ph21.append(ph + ph20)
        ph22.append(ph + ph20 + to180)
        ph23.append(-ph + ph20)
        ph24.append(-ph + ph20 + to180)

    phtxt = """
		
30m mc #0 to 2 F1PH(ip11*%d & ip12*%d & ip13*%d & ip14*%d, id0)

HaltAcqu, 1m
exit

;-------------------------------------------------------------------------------
; PHASELIST (computed externally when running the python program)
;
""" % (ip0, ip0, ip0, ip0)

    phtxt += "\nph1 = (4) "
    for phase in ph1:
        for _ in range(mq * 2):
            phtxt += "%d " % (phase)
    phtxt += "\n"  
         
    phtxt += "\nph11 = (65536) "
    for phase in ph11:
        phtxt += "%d " % round(phase % to360)
    phtxt += "\nph12 = (65536) "
    for phase in ph12:
        phtxt += "%d " % round(phase % to360) 
    phtxt += "\nph13 = (65536) "
    for phase in ph13:
        phtxt += "%d " % round(phase % to360) 
    phtxt += "\nph14 = (65536) "
    for phase in ph14:
        phtxt += "%d " % round(phase % to360) 
		
    phtxt += "\nph21 = (65536) "
    for phase in ph21:
        phtxt += "%d " % round(phase % to360)
    phtxt += "\nph22 = (65536) "
    for phase in ph22:
        phtxt += "%d " % round(phase % to360)
    phtxt += "\nph23 = (65536) "
    for phase in ph23:
        phtxt += "%d " % round(phase % to360)
    phtxt += "\nph24 = (65536) "
    for phase in ph24:
        phtxt += "%d " % round(phase % to360)
		
    phtxt += "\n\nph31 = ph21*%d + ph1" % mq
		
    return phtxt
			
	# end calcPhaseLists()

def enterParams():
    """
    Parameter dialog: Reads params. from TopSpin param. file,
    computes power, writes new params. back to file.
    returns - [N, n, nu, mq] when OK pressed, otherwise None
    """

    # nu_R
    nu_R = int(GETPAR("MASR"))
    
    # N, n, nu
    N = int(GETPAR("L 1"))
    n = int(GETPAR("L 2"))
    nu = int(GETPAR("L 3"))
    ns = int(GETPAR("NS"))
    phase0 = float(GETPAR("CNST 0"))
    
    # p90 and plw
    plw_ref = float(GETPAR("PLW 1"))
    p90_ref = float(GETPAR("P 1"))

    try:
        mq = int(GETPAR("L 4"))  # param. used to save experiment
    except:
        mq = 2  # double quantum by default

    result = INPUT_DIALOG(ppName, "Please enter parameters:",
                          ["MAS frequency (Hz)", "N =", "n =", "nu =", "mq recoupling order = ", "ph0 (deg)"],
                          [str(nu_R), str(N), str(n), str(nu), str(mq), str(phase0)], ["", "", "", "", "", ""],
                          ["1", "1", "1", "1", "1", "1", "1"], None, None, 15)

    if result == None:
        return None  # Cancel pressed

    nu_R = int(result[0])
    nu_R = min(max(1000., nu_R), 45000.)
		
    N = int(result[1])
    n = int(result[2])
    nu = int(result[3])
    mq = int(result[4])
    ns = int(ns // (mq * 2 * 4)) * (mq * 2 * 4)
    phase0 = float(result[5])
		
    plw = calcRNparam(nu_R, N, n, plw_ref, p90_ref)

    PUTPAR("L 4", str(mq))
    PUTPAR("L 1", str(N))
    PUTPAR("L 2", str(n))
    PUTPAR("L 3", str(nu))
    PUTPAR("CNST 31", str(nu_R))
    PUTPAR("MASR", str(nu_R))
    PUTPAR("PLW 11", str(plw))
    PUTPAR("NS", str(ns))
    PUTPAR("CNST 0", str(phase0))
    
    
    return N, n, nu, mq, plw, phase0
    # end def enterParams()

"""
Main program:
	- Displays parameter input dialog and computes power level
	- computes phase lists
	- optionally shows pulse program with phase lists or starts acquisition
"""
if CURDATA() == None:
    MSG("No dataset open")
    EXIT()

pp = DEF_PULSPROG(PPTEXT)  # define pulse progr. object

action = 0  # gets assigned the button number pressed
while 1:

    # open parameter input dialog
    ppName = "undefined"  # the pulsprog name for zg
    res = enterParams()
    if res == None:
        EXIT()  # Cancel pressed in parameter dialog, terminate program

    # extract results  
    N, n, nu, mq, plw, phase0 = res
    ppName = "%dQ_SQ_R%d_%d_%d.ppy"%(mq, N, n, nu)  # the pulsprog name for zg

    while 1:

        # open Power result dialog with 4 buttons
        action = SELECT(ppName, \
                        "Calculated power level for MQ excitation:\n\nPLW 11 "
                        "= " + str(plw),
                        ["View Pulse Program", "Start Acquisition", \
                         "Repeat Parameter Dialog", "Cancel"], \
                        ['v', 's', 'r', 'c'])
        if action == 0:
            # button 0 pressed
            phtxt = calcPhaseLists(N, n, nu, mq, phase0)
            VIEWTEXT("Pulse Program", "", pp.GET_TEXT()+phtxt)
            pp = DEF_PULSPROG(PPTEXT)  # reset so as not to append phases twice

        elif action == 1:
            # button 1 pressed
            phtxt = calcPhaseLists(N, n, nu, mq, phase0)
            pp = DEF_PULSPROG(PPTEXT + phtxt)  # add phase list            
            PUTPAR("PULPROG", ppName)
            pp.SAVE_AS(ppName)  # save pulse program for zg
            #ZG()
            EXIT()

        elif action == 2:
            # button 2 pressed
            break  # repeat parameter dialog

        elif action == 3:
            # button 3 pressed
            EXIT()
